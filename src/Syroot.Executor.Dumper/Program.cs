﻿using System;

namespace Syroot.Executor.Dumper
{
    /// <summary>
    /// Main class of the program containing the application entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
#if DEBUG
            args = new string[1];
            args[0] = @"C:\Windows\SysWOW64\shell32.dll";
            args[0] = @"C:\Windows\SysWOW64\calc.exe";
            args[0] = @"C:\Games\Worms Armageddon 3.6.31.0\dsound.dll";
            args[0] = @"C:\Games\KartRider\KartRider.exe";
#endif
            if (args.Length == 0)
                WriteUsage();
            else
                DumpFile(args[0]);
#if DEBUG
            Console.ReadLine();
#endif
        }

        private static void WriteUsage()
        {
            Console.WriteLine("Dumps the contents of a PE (portable executable) file.");
            Console.WriteLine();
            Console.WriteLine("DUMPER FileName");
            Console.WriteLine();
            Console.WriteLine("\tFileName\tThe name of the PE file to dump.");
        }

        // ---- Dump Methods ----

        private static void DumpFile(string fileName)
        {
            Console.WriteLine(String.Format("PE contents of \"{0}\":", fileName));

            PEFile peFile;
            try
            {
                peFile = new PEFile(fileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception when parsing the PE file:");
                Console.WriteLine(ex);
                return;
            }

            DumpDosHeader(peFile);
            DumpCoffHeader(peFile);
            DumpOptionalHeader(peFile);
            DumpDataDirectories(peFile);
            DumpSections(peFile);
        }

        private static void DumpDosHeader(PEFile peFile)
        {
            WriteHeader("DosHeader");
            WriteProperty(0, "LastPageByteCount", peFile.DosHeader.LastPageByteCount);
            WriteProperty(0, "PageCount", peFile.DosHeader.PageCount);
            WriteProperty(0, "RelocationCount", peFile.DosHeader.RelocationCount);
            WriteProperty(0, "HeaderSize", peFile.DosHeader.HeaderSize);
            WriteProperty(0, "MinAllocation", peFile.DosHeader.MinAllocation);
            WriteProperty(0, "MaxAllocation", peFile.DosHeader.MaxAllocation);
            WriteProperty(0, "InitialStackSegment", peFile.DosHeader.InitialStackSegment);
            WriteProperty(0, "InitialStackPointer", peFile.DosHeader.InitialStackPointer);
            WriteProperty(0, "Checksum", peFile.DosHeader.Checksum);
            WriteProperty(0, "InitialInstructionPointer", peFile.DosHeader.InitialInstructionPointer);
            WriteProperty(0, "InitialCodeSegment", peFile.DosHeader.InitialCodeSegment);
            WriteProperty(0, "RelocationTablePointer", peFile.DosHeader.RelocationTablePointer);
            WriteProperty(0, "OverlayNumber", peFile.DosHeader.OverlayNumber);
            WriteProperty(0, "OemId", peFile.DosHeader.OemId);
            WriteProperty(0, "OemInformation", peFile.DosHeader.OemInformation);
            WriteProperty(0, "PEOffset", peFile.DosHeader.PEOffset);
        }

        private static void DumpCoffHeader(PEFile peFile)
        {
            WriteHeader("CoffHeader");
            WriteProperty(0, "MachineType", peFile.CoffHeader.MachineType);
            WriteProperty(0, "SectionCount", peFile.CoffHeader.SectionCount);
            WriteProperty(0, "TimeDateStamp", peFile.CoffHeader.TimeDateStamp);
            WriteProperty(0, "SymbolTableOffset", peFile.CoffHeader.SymbolTableOffset);
            WriteProperty(0, "OptionalHeaderSize", peFile.CoffHeader.OptionalHeaderSize);
            WriteProperty(0, "Characteristics", peFile.CoffHeader.Characteristics);
        }

        private static void DumpOptionalHeader(PEFile peFile)
        {
            WriteHeader("OptionalHeader");
            WriteProperty(0, "PEFormat", peFile.OptionalHeader.PEFormat);
            WriteSubHeader("Standard properties");
            WriteProperty(1, "LinkerVersion", peFile.OptionalHeader.LinkerVersion);
            WriteProperty(1, "CodeSize", peFile.OptionalHeader.CodeSize);
            WriteProperty(1, "InitializedDataSize", peFile.OptionalHeader.InitializedDataSize);
            WriteProperty(1, "UninitializedDataSize", peFile.OptionalHeader.UninitializedDataSize);
            WriteProperty(1, "EntryPointRva", peFile.OptionalHeader.EntryPointRva);
            WriteProperty(1, "CodeBase", peFile.OptionalHeader.CodeBase);
            if (peFile.OptionalHeader.PEFormat == PEFormat.PE32)
            {
                WriteProperty(1, "DataBase", peFile.OptionalHeader.DataBase);
            }

            WriteSubHeader("Windows-specific properties");
            WriteProperty(1, "ImageBase", peFile.OptionalHeader.ImageBase);
            WriteProperty(1, "SectionAlignment", peFile.OptionalHeader.SectionAlignment);
            WriteProperty(1, "FileAlignment", peFile.OptionalHeader.FileAlignment);
            WriteProperty(1, "OperatingSystemVersion", peFile.OptionalHeader.OperatingSystemVersion);
            WriteProperty(1, "ImageVersion", peFile.OptionalHeader.ImageVersion);
            WriteProperty(1, "SubsystemVersion", peFile.OptionalHeader.SubsystemVersion);
            WriteProperty(1, "ImageSize", peFile.OptionalHeader.ImageSize);
            WriteProperty(1, "HeadersSize", peFile.OptionalHeader.HeadersSize);
            WriteProperty(1, "Checksum", peFile.OptionalHeader.Checksum);
            WriteProperty(1, "Subsystem", peFile.OptionalHeader.Subsystem);
            WriteProperty(1, "DllCharacteristics", peFile.OptionalHeader.DllCharacteristics);
            WriteProperty(1, "StackReserveSize", peFile.OptionalHeader.StackReserveSize);
            WriteProperty(1, "StackCommitSize", peFile.OptionalHeader.StackCommitSize);
            WriteProperty(1, "HeapReserveSize", peFile.OptionalHeader.HeapReserveSize);
            WriteProperty(1, "HeapCommitSize", peFile.OptionalHeader.HeapCommitSize);
            WriteProperty(1, "RvaCount", peFile.OptionalHeader.RvaCount);
        }

        private static void DumpDataDirectories(PEFile peFile)
        {
            for (int i = 0; i < peFile.OptionalHeader.RvaCount; i++)
            {
                // Write header
                DataDirectoryHeader header = peFile.DataDirectoryHeaders[i];
                if (header.Rva != 0 && header.Size != 0)
                {
                    WriteHeader(((DataDirectoryEntry)i).ToString());
                    WriteProperty(0, "Rva", header.Rva);
                    WriteProperty(0, "Size", header.Size);

                    // Write contents
                    switch (peFile.DataDirectories[i])
                    {
                        case ExportDataDirectory exportDataDirectory:
                            DumpExportDataDirectory(exportDataDirectory);
                            break;
                        case ImportDataDirectory importDataDirectory:
                            DumpImportDataDirectory(importDataDirectory);
                            break;
                        case ResourceDataDirectory resourceDataDirectory:
                            DumpResourceDataDirectory(resourceDataDirectory);
                            break;
                    }
                }
            }
        }

        private static void DumpExportDataDirectory(ExportDataDirectory exportDataDirectory)
        {
            WriteProperty(1, "TimeDateStamp", exportDataDirectory.TimeDateStamp);
            WriteProperty(1, "Version", exportDataDirectory.Version);
            WriteProperty(1, "DllNameRva", exportDataDirectory.DllNameRva);
            WriteProperty(1, "DllName", exportDataDirectory.DllName);
            WriteProperty(1, "OrdinalStartNumber", exportDataDirectory.OrdinalStartNumber);
            WriteProperty(1, "EntryCount", exportDataDirectory.EntryCount);
            WriteProperty(1, "NameEntryCount", exportDataDirectory.NameEntryCount);
            WriteProperty(1, "CodeAddressTableRva", exportDataDirectory.CodeAddressTableRva);
            WriteProperty(1, "NameAddressTableRva", exportDataDirectory.NameAddressTableRva);
            WriteProperty(1, "OrdinalTableRva", exportDataDirectory.OrdinalTableRva);

            // Export table header
            Console.WriteLine();
            WriteProperty(0,
                "#".PadRight(5)
                + "Hint".PadRight(5)
                + "Address".PadRight(11)
                + "Name".PadRight(40)
                + "ForwardedTo", String.Empty);
            foreach (ExportEntry export in exportDataDirectory.ExportEntries)
            {
                // Skip empty exports
                if (export.CodeOrForwarderRva > 0)
                {
                    Console.Write(export.Ordinal.ToString().PadRight(5));
                    Console.Write(export.Hint.ToString().PadRight(5));
                    Console.Write(("0x" + export.CodeOrForwarderRva.ToString("X8")).PadRight(11));
                    Console.Write(export.Name == null ? "<unnamed>".PadRight(40) : export.Name.PadRight(60));
                    Console.WriteLine(export.ForwarderName);
                }
            }
        }

        private static void DumpImportDataDirectory(ImportDataDirectory importDataDirectory)
        {
            foreach (ImportTable importTable in importDataDirectory.ImportTables)
            {
                Console.WriteLine();
                WriteProperty(0, importTable.DllName, String.Empty);
                WriteProperty(1, "ImportLookupTableRva", importTable.ImportLookupTableRva);
                WriteProperty(1, "ForwarderChain", importTable.ForwarderChain);
                WriteProperty(1, "DllNameRva", importTable.DllNameRva);
                WriteProperty(1, "ImportAddressTableRva", importTable.ImportAddressTableRva);

                // Import table header
                Console.WriteLine();
                WriteProperty(1,
                    "#".PadRight(5)
                    + "Hint".PadRight(5)
                    + "Name", String.Empty);
                foreach (ImportEntry import in importTable.Entries)
                {
                    Console.Write(new string(' ', 1 * 4)); // Indentation
                    Console.Write(import.Ordinal.ToString().PadRight(5));
                    Console.Write(import.Hint.ToString().PadRight(5));
                    Console.WriteLine(import.Name);
                }
            }
        }

        private static void DumpResourceDataDirectory(ResourceDataDirectory resourceDataDirectory)
        {
            Console.WriteLine();
            Console.WriteLine("Resources");
            for (int i = 0; i < resourceDataDirectory.RootNode.Nodes.Count; i++)
            {
                ResourceNode node = resourceDataDirectory.RootNode.Nodes[i];
                bool isLastNode = i == resourceDataDirectory.RootNode.Nodes.Count - 1;
                DumpResourceDataDirectoryNode(node, String.Empty, isLastNode);
            }
        }

        private static void DumpResourceDataDirectoryNode(ResourceNode node, string indent, bool isLastNode)
        {
            string nodeName = node.Name ?? node.Id.ToString();
            indent = WriteIndent(indent, nodeName, isLastNode);

            for (int i = 0; i < node.Nodes.Count; i++)
            {
                ResourceNode subNode = node.Nodes[i];
                isLastNode = i == node.Nodes.Count - 1;
                DumpResourceDataDirectoryNode(subNode, indent, isLastNode);
            }

            for (int i = 0; i < node.Leaves.Count; i++)
            {
                ResourceLeaf leaf = node.Leaves[i];
                isLastNode = i == node.Leaves.Count - 1;
                string leafName = leaf.Name ?? leaf.Id.ToString();
                WriteIndent(indent, leafName + " (DataSize=0x" + leaf.DataSize.ToString("X8") + ")", isLastNode);
            }
        }

        private static void DumpSections(PEFile peFile)
        {
            WriteHeader("Sections");
            for (int i = 0; i < peFile.SectionHeaders.Count; i++)
            {
                SectionHeader header = peFile.SectionHeaders[i];
                WriteSubHeader(String.Format("Section {0}: {1}", i + 1, header.Name));
                WriteProperty(1, "VirtualSize", header.VirtualSize);
                WriteProperty(1, "Rva", header.Rva);
                WriteProperty(1, "RawDataSize", header.RawDataSize);
                WriteProperty(1, "RawDataOffset", header.RawDataOffset);
                WriteProperty(1, "RelocationsOffset", header.RelocationsOffset);
                WriteProperty(1, "LineNumbersOffset", header.LineNumbersOffset);
                WriteProperty(1, "RelocationsCount", header.RelocationsCount);
                WriteProperty(1, "LineNumbersCount", header.LineNumbersCount);
                WriteProperty(1, "Characteristics", header.Characteristics);
            }
        }

        // ---- Helper Methods ----

        private static void WriteHeader(string name)
        {
            Console.WriteLine();
            Console.WriteLine((name + " ").PadRight(80, '-'));
        }

        private static void WriteSubHeader(string name)
        {
            Console.WriteLine();
            Console.WriteLine(name);
        }

        private static void WriteProperty(int indent, string name, object value)
        {
            Console.WriteLine(new string(' ', 4 * indent) + name.PadRight(30) + value.ToString());
        }

        private static string WriteIndent(string indent, string nodeText, bool isLastNode)
        {
            if (!String.IsNullOrEmpty(nodeText))
            {
                Console.Write(indent);
                if (isLastNode)
                {
                    // End the tree structure in this indent.
                    Console.Write("└───");
                    indent += "    ";
                }
                else
                {
                    // Continue the tree structure in this indent.
                    Console.Write("├───");
                    indent += "│   ";
                }

                Console.WriteLine(nodeText);
            }

            return indent;
        }
    }
}
