﻿using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents the structure of the COFF file header which contains information about the PE file.
    /// </summary>
    public class CoffHeader
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="CoffHeader"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        internal CoffHeader(Stream stream)
        {
            // Machine
            MachineType = stream.ReadEnum<MachineType>(true);
            SectionCount = stream.ReadUInt16();
            TimeDateStamp = stream.ReadDateTime(DateTimeCoding.CTime);

            // PointerToSymbolTable
            SymbolTableOffset = stream.ReadUInt32();
            if (SymbolTableOffset > 0)
            {
                if (SymbolTableOffset < stream.Position)
                    throw new PEFormatException($"Pointer to symbol table of {SymbolTableOffset} in front of COFF header.");
                else if (SymbolTableOffset > stream.Length)
                    throw new PEFormatException($"Pointer to symbol table of {SymbolTableOffset} beyond file length of {stream.Length}.");
            }

            SymbolCount = stream.ReadUInt32();
            OptionalHeaderSize = stream.ReadUInt16();
            Characteristics = (FileCharacteristics)stream.ReadUInt16();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the CPU type a machine requires to run the executable instructions.
        /// </summary>
        public MachineType MachineType { get; }

        /// <summary>
        /// Gets the number of sections in the executable file.
        /// </summary>
        public ushort SectionCount { get; }

        /// <summary>
        /// Gets a <see cref="DateTime"/> which indicates the point in time when the file was created.
        /// </summary>
        public DateTime TimeDateStamp { get; }

        /// <summary>
        /// Gets the offset in the file to the COFF symbol table, or 0 if no table exists.
        /// This value should be 0 for an image as COFF debugging information is deprecated.
        /// </summary>
        public uint SymbolTableOffset { get; }

        /// <summary>
        /// Gets the count of symbols in the COFF symbol table.
        /// This value should be 0 for an image as COFF debugging information is deprecated.
        /// </summary>
        public uint SymbolCount { get; }

        /// <summary>
        /// Gets the size of the optional header, which is required for executable files but not for object files.
        /// This value should be 0 for object files.
        /// </summary>
        public ushort OptionalHeaderSize { get; }

        /// <summary>
        /// Gets flags which provide additional information about the file.
        /// </summary>
        public FileCharacteristics Characteristics { get; }
    }

    /// <summary>
    /// Represents the CPU type a machine has. An image file can only be run on a machine which supports this CPU either
    /// natively or via emulation.
    /// </summary>
    /// <remarks>Native constants: IMAGE_FILE_MACHINE_*</remarks>
    public enum MachineType : ushort
    {
        /// <summary>
        /// Indicates compatibility to any machine.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_UNKNOWN</remarks>
        Unknown = 0x0000,

        /// <summary>
        /// Indicates compatibility to Intel 386 or later and compatible processors.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_I386</remarks>
        Intel386 = 0x014C,

        /// <summary>
        /// Indicates compatibility to MIPS little endian.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_R4000</remarks>
        Mips = 0x0166,

        /// <summary>
        /// Indicates compatibility to ARM little endian.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_ARM</remarks>
        Arm = 0x01C0,

        /// <summary>
        /// ARM v7 (or higher) in thumb mode only.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_ARMNT</remarks>
        ArmV7ThumbMode = 0x01C4,

        /// <summary>
        /// Indicates compatibility to Matsushita AM33.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_AM33</remarks>
        MatsushitaAM33 = 0x01D3,

        /// <summary>
        /// Indicates compatibility to Intel Itanium processor family.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_IA64</remarks>
        IntelItanium = 0x0200,

        /// <summary>
        /// Indicates compatibility to MIPS16.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_MIPS16</remarks>
        Mips16 = 0x0266,

        /// <summary>
        /// Indicates compatibility to MIPS with FPU.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_MIPSFPU</remarks>
        MipsFpu = 0x0366,

        /// <summary>
        /// Indicates compatibility to MIPS16 with FPU.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_MIPSFPU16</remarks>
        Mips16Fpu = 0x0466,

        /// <summary>
        /// Indicates compatibility to Hitachi SH3.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_SH3</remarks>
        HitachiSH3 = 0x01A2,

        /// <summary>
        /// Indicates compatibility to Hitachi SH3 DSP.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_SH3DSP</remarks>
        HitachiSH3Dsp = 0x01A3,

        /// <summary>
        /// Indicates compatibility to Hitachi SH4.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_SH4</remarks>
        HitachiSH4 = 0x01A6,

        /// <summary>
        /// Indicates compatibility to Hitachi SH5.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_SH5</remarks>
        HitachiSH5 = 0x01A8,

        /// <summary>
        /// Indicates compatibility to ARM or Thumb (interworking).
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_THUMB</remarks>
        ArmOrThumb = 0x01C2,

        /// <summary>
        /// Indicates compatibility to MIPS little endian WCE v2.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_WCEMIPSV2</remarks>
        MipsWce2 = 0x0169,

        /// <summary>
        /// Indicates compatibility to Power PC little endian.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_POWERPC</remarks>
        PowerPC = 0x01F0,

        /// <summary>
        /// Indicates compatibility to Power PC with floating point support.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_POWERPCFP</remarks>
        PowerPCFloat = 0x01F1,

        /// <summary>
        /// Indicates compatibility to EFI byte code.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_EBC</remarks>
        EfiByteCode = 0x0EBC,

        /// <summary>
        /// Indicates compatibility to x64 processors.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_AMD64</remarks>
        Amd64 = 0x8664,

        /// <summary>
        /// Indicates compatibility to Mitsubishi M32R little endian.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_M32R</remarks>
        MitsubishiM32R = 0x9041,

        /// <summary>
        /// Indicates compatibility to ARM v8 in 64-bit mode.
        /// </summary>
        /// <remarks>Native constant: IMAGE_FILE_MACHINE_ARM64</remarks>
        ArmV8In64BitMode = 0xAA64,
    }

    /// <summary>
    /// Represents attributes of the executable.
    /// </summary>
    [Flags]
    public enum FileCharacteristics : ushort
    {
        /// <summary>
        /// Indicates that the file does not contain base relocations, and thus must be loaded at its preferred base
        /// address.
        /// </summary>
        RelocationsStripped = 0x0001,

        /// <summary>
        /// Indicates that this file is a runnable executable.
        /// </summary>
        ExecutableImage = 0x0002,

        /// <summary>
        /// Deprecated; indicates that COFF line numbers have been removed.
        /// </summary>
        LineNumbersStripped = 0x0004,

        /// <summary>
        /// Deprecated; indicates that COFF symbol table entries for local symbols have been removed.
        /// </summary>
        LocalSymbolsStripped = 0x0008,

        /// <summary>
        /// Deprecated; indicates aggressive trimming of the working set.
        /// </summary>
        AggressiveWorkingSetTrim = 0x0010,

        /// <summary>
        /// Indicates that the application can handle addresses above 2 GB.
        /// </summary>
        LargeAddressAware = 0x0020,

        /// <summary>
        /// Deprecated; indicates little endian byte order.
        /// </summary>
        BytesReversedLow = 0x0080,

        /// <summary>
        /// Indicates that the machine is based on a 32-bit-word architecture.
        /// </summary>
        MachineIs32Bit = 0x0100,

        /// <summary>
        /// Indicates that debugging information was removed from the file.
        /// </summary>
        DebugStripped = 0x0200,

        /// <summary>
        /// Indicates that the file is completely loaded into memory if it is run from removable drives.
        /// </summary>
        RemovableRunFromSwap = 0x0400,

        /// <summary>
        /// Indicates that the file is completely loaded into memory if it is run from network drives.
        /// </summary>
        NetRunFromSwap = 0x0800,

        /// <summary>
        /// Indicates that the file is a system file rather than a user program.
        /// </summary>
        System = 0x1000,

        /// <summary>
        /// Indicates that the file is a dynamic link library rather and cannot be run directly.
        /// </summary>
        DynamicLinkLibrary = 0x2000,

        /// <summary>
        /// Indicates that the file should only be run on a uniprocessor machine.
        /// </summary>
        Uniprocessor = 0x4000,

        /// <summary>
        /// Deprecated; indicates big endian byte order.
        /// </summary>
        BytesReversedHigh = 0x8000
    }
}
