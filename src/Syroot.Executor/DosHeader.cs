﻿using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents the structure of the DOS header at the beginning of a PE file, containing information about the DOS
    /// (stub) program.
    /// </summary>
    /// <remarks>Native structure: IMAGE_DOS_HEADER</remarks>
    public class DosHeader
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="DosHeader"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        internal DosHeader(Stream stream)
        {
            // Check DOS magic header bytes.
            byte[] dosSignature = stream.ReadBytes(2);
            if (!dosSignature.Compare("MZ", Encoding.ASCII))
                throw new PEFormatException($"DOS signature of {Encoding.ASCII.GetString(dosSignature)} invalid.");

            LastPageByteCount = stream.ReadUInt16();
            PageCount = stream.ReadUInt16();
            RelocationCount = stream.ReadUInt16();
            HeaderSize = stream.ReadUInt16();
            MinAllocation = stream.ReadUInt16();
            MaxAllocation = stream.ReadUInt16();
            InitialStackSegment = stream.ReadUInt16();
            InitialStackPointer = stream.ReadUInt16();
            Checksum = stream.ReadUInt16();
            InitialInstructionPointer = stream.ReadUInt16();
            InitialCodeSegment = stream.ReadUInt16();
            RelocationTablePointer = stream.ReadUInt16();
            OverlayNumber = stream.ReadUInt16();
            Reserved1 = stream.ReadUInt16();
            Reserved2 = stream.ReadUInt16();
            Reserved3 = stream.ReadUInt16();
            Reserved4 = stream.ReadUInt16();
            OemId = stream.ReadUInt16();
            OemInformation = stream.ReadUInt16();
            Reserved2_1 = stream.ReadUInt16();
            Reserved2_2 = stream.ReadUInt16();
            Reserved2_3 = stream.ReadUInt16();
            Reserved2_4 = stream.ReadUInt16();
            Reserved2_5 = stream.ReadUInt16();
            Reserved2_6 = stream.ReadUInt16();
            Reserved2_7 = stream.ReadUInt16();
            Reserved2_8 = stream.ReadUInt16();
            Reserved2_9 = stream.ReadUInt16();
            Reserved2_10 = stream.ReadUInt16();

            // PEOffset
            PEOffset = stream.ReadUInt32();
            if (PEOffset > stream.Length)
                throw new PEFormatException($"PE offset of {PEOffset} beyond file length of {stream.Length}.");

            // TODO: Get additional information about the DOS (stub) program.
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the number of bytes on the last page of the file.
        /// </summary>
        /// <remarks>Native field: e_cblp</remarks>
        public ushort LastPageByteCount { get; }

        /// <summary>
        /// Gets the number of pages in the file.
        /// </summary>
        /// <remarks>Native field: e_cp</remarks>
        public ushort PageCount { get; }

        /// <summary>
        /// Gets the number of relocations.
        /// </summary>
        /// <remarks>Native field: e_crlc</remarks>
        public ushort RelocationCount { get; }

        /// <summary>
        /// Gets the size of the header in paragraphs.
        /// </summary>
        /// <remarks>Native field: e_cparhdr</remarks>
        public ushort HeaderSize { get; }

        /// <summary>
        /// Gets the minimum paragraphs to allocate.
        /// </summary>
        /// <remarks>Native field: e_minalloc</remarks>
        public ushort MinAllocation { get; }

        /// <summary>
        /// Gets the maximum paragraphs to allocate.
        /// </summary>
        /// <remarks>Native field: e_maxalloc</remarks>
        public ushort MaxAllocation { get; }

        /// <summary>
        /// Gets the initial SS to set by the loader.
        /// </summary>
        /// <remarks>Native field: e_ss</remarks>
        public ushort InitialStackSegment { get; }

        /// <summary>
        /// Gets the initial SP to set by the loader.
        /// </summary>
        /// <remarks>Native field: e_sp</remarks>
        public ushort InitialStackPointer { get; }

        /// <summary>
        /// Gets the checksum of the file.
        /// </summary>
        /// <remarks>Native field: e_csum</remarks>
        public ushort Checksum { get; }

        /// <summary>
        /// Gets the initial IP to set by the loader.
        /// </summary>
        /// <remarks>Native field: e_ip</remarks>
        public ushort InitialInstructionPointer { get; }

        /// <summary>
        /// Gets the initial CS to set by the loader.
        /// </summary>
        /// <remarks>Native field: e_cs</remarks>
        public ushort InitialCodeSegment { get; }

        /// <summary>
        /// Gets the address of the relocation table.
        /// </summary>
        /// <remarks>Native field: e_lfarlc</remarks>
        public ushort RelocationTablePointer { get; }

        /// <summary>
        /// Gets the overlay number.
        /// </summary>
        /// <remarks>Native field: e_ovno</remarks>
        public ushort OverlayNumber { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res[0]</remarks>
        public ushort Reserved1 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res[1]</remarks>
        public ushort Reserved2 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res[2]</remarks>
        public ushort Reserved3 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res[3]</remarks>
        public ushort Reserved4 { get; }

        /// <summary>
        /// Gets the OEM ID.
        /// </summary>
        /// <remarks>Native field: e_oemid</remarks>
        public ushort OemId { get; }

        /// <summary>
        /// Gets the OEM information.
        /// </summary>
        /// <remarks>Native field: e_oeminfo</remarks>
        public ushort OemInformation { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[0]</remarks>
        public ushort Reserved2_1 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[1]</remarks>
        public ushort Reserved2_2 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[2]</remarks>
        public ushort Reserved2_3 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[3]</remarks>
        public ushort Reserved2_4 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[4]</remarks>
        public ushort Reserved2_5 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[5]</remarks>
        public ushort Reserved2_6 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[6]</remarks>
        public ushort Reserved2_7 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[7]</remarks>
        public ushort Reserved2_8 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[8]</remarks>
        public ushort Reserved2_9 { get; }

        /// <summary>
        /// Gets a reserved property.
        /// </summary>
        /// <remarks>Native field: e_res2[9]</remarks>
        public ushort Reserved2_10 { get; }

        /// <summary>
        /// Gets the offset to the PE header signature.
        /// </summary>
        /// <remarks>Native field: e_lfanew</remarks>
        public uint PEOffset { get; }
    }
}
