﻿using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a portable executable file.
    /// </summary>
    public class PEFile
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PEFile"/> class, reading data from the file with the given
        /// file name.
        /// </summary>
        /// <param name="fileName">The name of the file to represent.</param>
        public PEFile(string fileName)
            : this(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read), false) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PEFile"/> class, reading data from the given stream which may
        /// be closed after a successful read.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the PE file from.</param>
        /// <param name="leaveOpen"><see langword="true"/> to leave <paramref name="stream"/> open after reading data.
        /// </param>
        public PEFile(Stream stream, bool leaveOpen)
        {
            try
            {
                DosHeader = new DosHeader(stream);

                // Jump to the PE offset specified in the DOS header and compare the signature.
                stream.Seek(DosHeader.PEOffset, SeekOrigin.Begin);
                byte[] signature = stream.ReadBytes(4);
                if (!signature.Compare("PE\0\0", Encoding.ASCII))
                    throw new PEFormatException($"PE header signature of \"{Encoding.ASCII.GetString(signature)}\" invalid.");

                CoffHeader = new CoffHeader(stream);
                OptionalHeader = new OptionalHeader(stream);

                // Read the data directory headers.
                DataDirectoryHeader[] dataDirectoryHeaders = new DataDirectoryHeader[OptionalHeader.RvaCount];
                for (int i = 0; i < OptionalHeader.RvaCount; i++)
                    dataDirectoryHeaders[i] = new DataDirectoryHeader(stream);
                DataDirectoryHeaders = new ReadOnlyCollection<DataDirectoryHeader>(dataDirectoryHeaders);

                // Read the section table.
                SectionHeader[] sectionHeaders = new SectionHeader[CoffHeader.SectionCount];
                for (uint i = 0; i < CoffHeader.SectionCount; i++)
                    sectionHeaders[i] = new SectionHeader(stream);
                SectionHeaders = new ReadOnlyCollection<SectionHeader>(sectionHeaders);

                // After reading the sections, it is now possible to fill the data of each directory entry.
                DataDirectoryBase[] dataDirectories = new DataDirectoryBase[OptionalHeader.RvaCount];
                for (int i = 0; i < OptionalHeader.RvaCount; i++)
                {
                    // If directory header exists, instantiate data of it in the correct type.
                    DataDirectoryHeader directoryHeader = DataDirectoryHeaders[i];
                    if (directoryHeader.Rva != 0 && directoryHeader.Size != 0)
                        dataDirectories[i] = DataDirectoryBase.Instantiate(stream, this, (DataDirectoryEntry)i, directoryHeader);
                }
                DataDirectories = new ReadOnlyCollection<DataDirectoryBase>(dataDirectories);
            }
            finally
            {
                if (!leaveOpen)
                    stream.Dispose();
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the DOS header specifying information about the DOS (stub) program.
        /// </summary>
        public DosHeader DosHeader { get; }

        /// <summary>
        /// Gets the COFF header specifying information about the PE file.
        /// </summary>
        public CoffHeader CoffHeader { get; }

        /// <summary>
        /// Gets the optional header, which is required for image files, and provides information to the loader.
        /// </summary>
        public OptionalHeader OptionalHeader { get; }

        /// <summary>
        /// Gets the array of <see cref="DataDirectoryHeader">DataDirectories</see> loaded into memory with this image.
        /// </summary>
        public ReadOnlyCollection<DataDirectoryHeader> DataDirectoryHeaders { get; }

        /// <summary>
        /// Gets the data directory blocks, each strongly typed to match its contents.
        /// </summary>
        public ReadOnlyCollection<DataDirectoryBase> DataDirectories { get; }

        /// <summary>
        /// Gets the section table containing section header entries.
        /// </summary>
        public ReadOnlyCollection<SectionHeader> SectionHeaders { get; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Converts the given relative virtual address into a file offset by inspecting the section headers.
        /// </summary>
        /// <param name="relativeVirtualAddress">The relative virtual address to convert.</param>
        /// <returns>The offset in the file, or 0 if no section contains the RVA or the RVA is 0.</returns>
        internal uint GetFileOffset(uint relativeVirtualAddress)
        {
            if (relativeVirtualAddress > 0)
            {
                // Go through each section and check if the relative virtual address lies within.
                foreach (SectionHeader sectionHeader in SectionHeaders)
                {
                    if (relativeVirtualAddress >= sectionHeader.Rva
                        && relativeVirtualAddress < sectionHeader.Rva + sectionHeader.VirtualSize)
                    {
                        // Add the offset to the section start in the file rather than in address.
                        return relativeVirtualAddress - sectionHeader.Rva + sectionHeader.RawDataOffset;
                    }
                }
            }
            return 0;
        }
    }
}
