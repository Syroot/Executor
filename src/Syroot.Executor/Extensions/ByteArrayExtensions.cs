﻿using System.Text;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a collection of static extension method for objects of the byte array type.
    /// </summary>
    internal static class ByteArrayExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Compares the array with the given one and returns true if all bytes are equal, or false if not.
        /// </summary>
        /// <param name="t">The extended byte array.</param>
        /// <param name="array">The byte array to compare with.</param>
        /// <returns>true, if all bytes are equal; otherwise false.</returns>
        internal static bool Compare(this byte[] t, byte[] array)
        {
            if (array == null || t.Length != array.Length)
                return false;

            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] != array[i])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Compares the array with the given string in the specified encoding and returns true if all resulting bytes
        /// are equal, or false if not.
        /// </summary>
        /// <param name="t">The extended byte array.</param>
        /// <param name="text">The text in ASCII encoding to compare with.</param>
        /// <param name="encoding">The encoding to use to convert the string into a byte array.</param>
        /// <returns>true, if all resulting bytes are equal; otherwise false.</returns>
        internal static bool Compare(this byte[] t, string text, Encoding encoding)
        {
            return Compare(t, encoding.GetBytes(text));
        }
    }
}
