﻿using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents the optional PE header which is actually required for executable files, but not for object files.
    /// It contains additional information to the loader.
    /// </summary>
    public class OptionalHeader
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionalHeader"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        internal OptionalHeader(Stream stream)
        {
            // ---- Standard fields ----
            PEFormat = stream.ReadEnum<PEFormat>(true);
            LinkerVersion = new Version(stream.ReadByte(), stream.ReadByte());
            CodeSize = stream.ReadUInt32();
            InitializedDataSize = stream.ReadUInt32();
            UninitializedDataSize = stream.ReadUInt32();
            EntryPointRva = stream.ReadUInt32();
            CodeBase = stream.ReadUInt32();

            // BaseOfData
            if (PEFormat == PEFormat.PE32)
                DataBase = stream.ReadUInt32();

            // ---- Windows-specific fields ----
            // ImageBase (multiple of 64K)
            ImageBase = PEFormat == PEFormat.PE32Plus ? stream.ReadUInt64() : stream.ReadUInt32();
            if (ImageBase % 0x10000 != 0)
                throw new PEFormatException($"Image base of {ImageBase} is not a multiple of 64K.");

            SectionAlignment = stream.ReadUInt32();
            FileAlignment = stream.ReadUInt32();
            if (SectionAlignment < FileAlignment)
                throw new PEFormatException($"Section alignment of {FileAlignment} is smaller than the file alignment of {SectionAlignment}.");

            OperatingSystemVersion = new Version(stream.ReadUInt16(), stream.ReadUInt16());
            ImageVersion = new Version(stream.ReadUInt16(), stream.ReadUInt16());
            SubsystemVersion = new Version(stream.ReadUInt16(), stream.ReadUInt16());
            Win32VersionValue = stream.ReadUInt32();

            // ImageSize (multiple of SectionAlignment)
            ImageSize = stream.ReadUInt32();
            if (ImageSize % SectionAlignment != 0)
                throw new PEFormatException($"Image size of {ImageSize} is not a multiple of the section alignment of {SectionAlignment}.");

            // HeaderSize (multiple of FileAlignment)
            HeadersSize = stream.ReadUInt32();
            if (HeadersSize % FileAlignment != 0)
                throw new PEFormatException($"Header size of {ImageSize} is not a multiple of the file alignment of {FileAlignment}.");

            Checksum = stream.ReadUInt32();
            Subsystem = stream.ReadEnum<WindowsSubsystem>(true);
            DllCharacteristics = (DllCharacteristics)stream.ReadUInt16();
            StackReserveSize = PEFormat == PEFormat.PE32Plus ? stream.ReadUInt64() : stream.ReadUInt32();
            StackCommitSize = PEFormat == PEFormat.PE32Plus ? stream.ReadUInt64() : stream.ReadUInt32();
            HeapReserveSize = PEFormat == PEFormat.PE32Plus ? stream.ReadUInt64() : stream.ReadUInt32();
            HeapCommitSize = PEFormat == PEFormat.PE32Plus ? stream.ReadUInt64() : stream.ReadUInt32();
            LoaderFlags = stream.ReadUInt32();
            RvaCount = stream.ReadUInt32();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        // ---- Standard properties ----

        /// <summary>
        /// Gets the PE executable format of the image.
        /// </summary>
        public PEFormat PEFormat { get; }

        /// <summary>
        /// Gets the linker version number.
        /// </summary>
        public Version LinkerVersion { get; }

        /// <summary>
        /// Gets the size of the code (text) section, or the sum of all code sections if there are multiple sections.
        /// </summary>
        public uint CodeSize { get; }

        /// <summary>
        /// Gets the size of the initialized data section, or the sum of all such sections if there are multiple data
        /// sections.
        /// </summary>
        public uint InitializedDataSize { get; }

        /// <summary>
        /// Gets the size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple
        /// BSS sections.
        /// </summary>
        public uint UninitializedDataSize { get; }

        /// <summary>
        /// Gets the RVA of the entry point when the executable file is loaded into memory. For program images, this is
        /// the starting address. For device drivers, this is the address of the initialization function. An entry point
        /// is optional for DLLs. When no entry point is present, this property must be zero.
        /// </summary>
        public uint EntryPointRva { get; }

        /// <summary>
        /// Gets the address that is relative to the image base of the beginning-of-code section when it is loaded into
        /// memory.
        /// </summary>
        public uint CodeBase { get; }

        /// <summary>
        /// Gets the address that is relative to the image base of the beginning-of-data section when it is loaded into
        /// memory.
        /// Only present in PE32.
        /// </summary>
        public uint DataBase { get; }

        // ---- Windows-specific properties ----

        /// <summary>
        /// Gets the preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K.
        /// The default for DLLs is 0x10000000. The default for Windows CE EXEs is 0x00010000. The default for Windows
        /// NT, Windows 2000, Windows XP, Windows 95, Windows 98, and Windows Me is 0x00400000.
        /// </summary>
        public ulong ImageBase { get; }

        /// <summary>
        /// Gets the alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal
        /// to <see cref="FileAlignment"/>. The default is the page size for the architecture.
        /// </summary>
        public uint SectionAlignment { get; }

        /// <summary>
        /// Gets the alignment factor (in bytes) that is used to align the raw data of sections in the image file. The
        /// value should be a power of 2 between 512 and 64 K, inclusive. The default is 512. If the
        /// <see cref="SectionAlignment"/> is less than the architecture's page size, then <see cref="FileAlignment"/>
        /// must match <see cref="SectionAlignment"/>.
        /// </summary>
        public uint FileAlignment { get; }

        /// <summary>
        /// Gets the version number of the required operating system.
        /// </summary>
        public Version OperatingSystemVersion { get; }

        /// <summary>
        /// Gets the version number of the image.
        /// </summary>
        public Version ImageVersion { get; }

        /// <summary>
        /// Gets the version number of the subsystem.
        /// </summary>
        public Version SubsystemVersion { get; }

        /// <summary>
        /// Gets a reserved property which should be 0.
        /// </summary>
        public uint Win32VersionValue { get; }

        /// <summary>
        /// Gets size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a
        /// multiple of <see cref="SectionAlignment"/>.
        /// </summary>
        public uint ImageSize { get; }

        /// <summary>
        /// Gets the combined size of an MS DOS stub, PE header, and section headers rounded up to a multiple of
        /// <see cref="FileAlignment"/>.
        /// </summary>
        public uint HeadersSize { get; }

        /// <summary>
        /// Gets the image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL.
        /// The following are checked for validation at load time: all drivers, any DLL loaded at boot time, and any DLL
        /// that is loaded into a critical Windows process.
        /// </summary>
        public uint Checksum { get; }

        /// <summary>
        /// Gets the subsystem that is required to run this image.
        /// </summary>
        public WindowsSubsystem Subsystem { get; }

        /// <summary>
        /// Gets the attributes with which the image is decorated.
        /// </summary>
        public DllCharacteristics DllCharacteristics { get; }

        /// <summary>
        /// Gets the size of the stack to reserve. Only <see cref="StackCommitSize"/> is committed; the rest is made
        /// available one page at a time until the reserve size is reached.
        /// </summary>
        public ulong StackReserveSize { get; }

        /// <summary>
        /// Gets the size of the stack to commit.
        /// </summary>
        public ulong StackCommitSize { get; }

        /// <summary>
        /// Gets the size of the local heap space to reserve. Only <see cref="HeapCommitSize"/> is committed; the rest
        /// is made available one page at a time until the reserve size is reached.
        /// </summary>
        public ulong HeapReserveSize { get; }

        /// <summary>
        /// Gets the size of the local heap space to commit.
        /// </summary>
        public ulong HeapCommitSize { get; }

        /// <summary>
        /// Gets a reserved collection of flags for the Windows PE loader which should be 0.
        /// </summary>
        public uint LoaderFlags { get; }

        /// <summary>
        /// Gets the number of data directory entries in the remainder of the optional header. Each describes a location
        /// and size.
        /// </summary>
        public uint RvaCount { get; }
    }

    /// <summary>
    /// Represents the valid PE file formats.
    /// </summary>
    /// <remarks>Native constants: IMAGE_*_OPTIONAL_HDR*_MAGIC</remarks>
    public enum PEFormat : ushort
    {
        /// <summary>
        /// Indicates a ROM image.
        /// </summary>
        /// <remarks>Native constant: IMAGE_ROM_OPTIONAL_HDR_MAGIC</remarks>
        Rom = 0x0107,

        /// <summary>
        /// Indicates a format for 32-bit applications.
        /// </summary>
        /// <remarks>Native constant: IMAGE_NT_OPTIONAL_HDR32_MAGIC</remarks>
        PE32 = 0x010B,

        /// <summary>
        /// Indicates a format for 64-bit applications, based on <see cref="PE32"/>, but with several changes to become
        /// capable of addressing 64-bit address space and other modifications.
        /// </summary>
        /// <remarks>Native constant: IMAGE_NT_OPTIONAL_HDR64_MAGIC</remarks>
        PE32Plus = 0x020B
    }

    /// <summary>
    /// Represents the subsystems required to run the image.
    /// </summary>
    /// <remarks>Native constants: IMAGE_SUBSYSTEM_*</remarks>
    public enum WindowsSubsystem : ushort
    {
        /// <summary>
        /// Indicates an unknown subsystem.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_UNKNOWN</remarks>
        Unknown = 0,

        /// <summary>
        /// Indicates device drivers and native Windows processes.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_NATIVE</remarks>
        Native = 1,

        /// <summary>
        /// Indicates a Windows application with a graphical user interface.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_WINDOWS_GUI</remarks>
        Gui = 2,

        /// <summary>
        /// Indicates a Windows application with a console user interface.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_WINDOWS_CUI</remarks>
        Cui = 3,

        /// <summary>
        /// Indicates a OS/2 application with a console user interface.
        /// This flag is not officially documented.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_OS2_CUI</remarks>
        OS2Cui = 5,

        /// <summary>
        /// Indicates a POSIX application with a console user interface.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_POSIX_CUI</remarks>
        PosixCui = 7,

        /// <summary>
        /// Indicates native Windows 9x device drivers.
        /// This flag is not officially documented.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_NATIVE_WINDOWS</remarks>
        NativeWindows = 8,

        /// <summary>
        /// Indicates a Windows CE application with a graphical user interface.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_WINDOWS_CE_GUI</remarks>
        WindowsCEGui = 9,

        /// <summary>
        /// Indicates an EFI application.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_EFI_APPLICATION</remarks>
        EfiApplication = 10,

        /// <summary>
        /// Indicates an EFI driver with boot services.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER</remarks>
        EfiBootServiceDriver = 11,

        /// <summary>
        /// Indicates an EFI driver with run-time services.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_EFI_RUNTIME_DRIVER</remarks>
        EfiRuntimeDriver = 12,

        /// <summary>
        /// Indicates an EFI ROM image.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_EFI_ROM</remarks>
        EfiRom = 13,

        /// <summary>
        /// Indicates an XBox application.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_XBOX</remarks>
        Xbox = 14,

        /// <summary>
        /// Indicates a Windows boot application.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SUBSYSTEM_WINDOWS_BOOT_APPLICATION</remarks>
        BootApplication = 16
    }

    /// <summary>
    /// Represents a set of attributes an image can have.
    /// </summary>
    /// <remarks>Native constants: IMAGE_DLL_CHARACTERISTICS_*</remarks>
    [Flags]
    public enum DllCharacteristics : ushort
    {
        /// <summary>
        /// Indicates a reserved flag of the value 0x0001.
        /// </summary>
        Reserved0x0001 = 0x0001,

        /// <summary>
        /// Indicates a reserved flag of the value 0x0002.
        /// </summary>
        Reserved0x0002 = 0x0002,

        /// <summary>
        /// Indicates a reserved flag of the value 0x0004.
        /// </summary>
        Reserved0x0004 = 0x0004,

        /// <summary>
        /// Indicates a reserved flag of the value 0x0008.
        /// </summary>
        Reserved0x0008 = 0x0008,

        /// <summary>
        /// Indicates an undocumented flag of the value 0x0010.
        /// </summary>
        Unknown0x0010 = 0x0010,

        /// <summary>
        /// Indicates that the executable image supports high-entropy 64-bit address space layout randomization (ASLR).
        /// This flag is not officially documented.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_HIGH_ENTROPY_VA</remarks>
        HighEntropyAslr = 0x0020,

        /// <summary>
        /// Indicates that the DLL can be relocated at load time.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_DYNAMIC_BASE</remarks>
        DynamicBase = 0x0040,

        /// <summary>
        /// Indicates that code integrity checks are enforced.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_FORCE_INTEGRITY</remarks>
        ForceIntegrity = 0x0080,

        /// <summary>
        /// Indicates that the image is compatible with the No-Execute-Byte.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_NX_COMPAT</remarks>
        NXCompatible = 0x0100,

        /// <summary>
        /// Indicates that the image is isolation aware, but should not be isolated.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_NO_ISOLATION</remarks>
        NoIsolation = 0x0200,

        /// <summary>
        /// Indicates that structured exception handling may not be used for this image.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_NO_SEH</remarks>
        NoSeh = 0x0400,

        /// <summary>
        /// Indicates that the image must not be bound.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_NO_BIND</remarks>
        NoBind = 0x0800,

        /// <summary>
        /// Indicates that the image should execute in an AppContainer, which is an import for Windows Store compatible
        /// applications.
        /// This flag is officially documented as reserved.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_APPCONTAINER</remarks>
        AppContainer = 0x1000,

        /// <summary>
        /// Indicates a WDM driver.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_WDM_DRIVER</remarks>
        WdmDriver = 0x2000,

        /// <summary>
        /// Indicates that the executable image supports Control Flow Guard.
        /// This flag is officially documented as reserved.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_GUARD_CF</remarks>
        EnableAslr = 0x4000,

        /// <summary>
        /// Indicates terminal server awareness.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DLL_CHARACTERISTICS_TERMINAL_SERVER_AWARE</remarks>
        TerminalServerAware = 0x8000
    }
}
