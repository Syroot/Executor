﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a section header specifying information about a section in the image.
    /// </summary>
    [DebuggerDisplay("Name = {Name}, Characteristics = {Characteristics}")]
    public class SectionHeader
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private string _originalName;
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private string _resolvedName;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SectionHeader"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        internal SectionHeader(Stream stream)
        {
            Name = Encoding.UTF8.GetString(stream.ReadBytes(8));
            VirtualSize = stream.ReadUInt32();
            Rva = stream.ReadUInt32();
            RawDataSize = stream.ReadUInt32();

            // RawDataPointer
            RawDataOffset = stream.ReadUInt32();
            if (RawDataOffset > stream.Length)
                throw new PEFormatException("Raw data pointer to section \"{0}\" of {0} beyond file length of {2}.",
                    OriginalName, RawDataOffset, stream.Length);
            if (RawDataOffset + RawDataSize > stream.Length)
                throw new PEFormatException("End of section \"{0}\" at {1} beyond file length of {2}.",
                    OriginalName, RawDataOffset + RawDataSize, stream.Length);

            RelocationsOffset = stream.ReadUInt32();
            LineNumbersOffset = stream.ReadUInt32();
            RelocationsCount = stream.ReadUInt16();
            LineNumbersCount = stream.ReadUInt16();
            Characteristics = (SectionCharacteristics)stream.ReadUInt32();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the (resolved) name of the section.
        /// </summary>
        public string Name
        {
            get => _resolvedName ?? _originalName;
            private set
            {
                _originalName = value;
                _resolvedName = null;
                if (_originalName != null)
                {
                    // Cut off trailing 0's.
                    _originalName = _originalName.TrimEnd('\0');

                    // Check if the original name points into the string table and resolve the name from there.
                    if (_originalName.Contains("/"))
                    {
                        string stringNumberText = _originalName.Substring(_originalName.IndexOf('/') + 1).TrimEnd('\0');
                        int stringNumber = -1;
                        if (Int32.TryParse(stringNumberText, out stringNumber))
                        {
                            // TODO: Resolve name from string table.
                            _resolvedName = _originalName;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the original name of the section, which is the same as <see cref="Name"/>, as long as
        /// <see cref="Name"/> does not point into the string table from which it would then be resolved.
        /// </summary>
        public string OriginalName => _originalName;

        /// <summary>
        /// Gets the total size of the section when loaded into memory. If this value is greater than
        /// <see cref="RawDataSize"/>, the section is zero-padded. This property is valid only for executable images and
        /// should be set to zero for object files.
        /// </summary>
        public uint VirtualSize { get; }

        /// <summary>
        /// Gets the address of the first byte of the section relative to the image base when the section is loaded into
        /// memory (for executable files). For object files, this property is the address of the first byte before
        /// relocation is applied; for simplicity, compilers should set this to zero. Otherwise, it is an arbitrary
        /// value that is subtracted from offsets during relocation.
        /// </summary>
        public uint Rva { get; }

        /// <summary>
        /// Gets the size of the section (for object files) or the size of the initialized data on disk (for image
        /// files). For executable images, this must be a multiple of <see cref="OptionalHeader.FileAlignment"/>. If
        /// this is less than <see cref="VirtualSize"/>, the remainder of the section is zero-filled. Because the
        /// <see cref="RawDataSize"/> property is rounded but the <see cref="VirtualSize"/> property is not, it is
        /// possible for <see cref="RawDataSize"/> to be greater than <see cref="VirtualSize"/> as well. When a section
        /// contains only uninitialized data, this property should be zero.
        /// </summary>
        public uint RawDataSize { get; }

        /// <summary>
        /// Gets the file pointer to the first page of the section within the COFF file. For executable images, this
        /// must be a multiple of <see cref="OptionalHeader.FileAlignment"/>. For object files, the value should be
        /// aligned on a 4-byte boundary for best performance. When a section contains only uninitialized data, this
        /// field should be zero.
        /// </summary>
        public uint RawDataOffset { get; }

        /// <summary>
        /// Gets the file pointer to the beginning of relocation entries for the section. This is set to zero for
        /// executable images or if there are no relocations.
        /// </summary>
        public uint RelocationsOffset { get; }

        /// <summary>
        /// Gets the file pointer to the beginning of line-number entries for the section. This is set to zero if there
        /// are no COFF line numbers. This value should be zero for an image because COFF debugging information is
        /// deprecated.
        /// </summary>
        public uint LineNumbersOffset { get; }

        /// <summary>
        /// Gets the number of relocation entries for the section. This is set to zero for executable images.
        /// </summary>
        public uint RelocationsCount { get; }

        /// <summary>
        /// Gets the number of line-number entries for the section. This value should be zero for an image because COFF
        /// debugging information is deprecated.
        /// </summary>
        public uint LineNumbersCount { get; }

        /// <summary>
        /// Gets the flags that describe the characteristics of the section.
        /// </summary>
        public SectionCharacteristics Characteristics { get; }
    }

    /// <summary>
    /// Represents the flags that describe the attributes with which a section is decorated.
    /// </summary>
    /// <remarks>Native constants: IMAGE_SCN_*</remarks>
    [Flags]
    public enum SectionCharacteristics : uint
    {
        /// <summary>
        /// Indicates a reserved flag of the value 0x00000001.
        /// </summary>
        Reserved0x00000001 = 0x00000001,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00000002.
        /// </summary>
        Reserved0x00000002 = 0x00000002,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00000004.
        /// </summary>
        Reserved0x00000004 = 0x00000004,

        /// <summary>
        /// Indicates that the section should not be padded to the next boundary. This flag is obsolete and is replaced
        /// by <see cref="Align1Bytes"/>. This is valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_TYPE_NO_PAD</remarks>
        NotPaddable = 0x00000008,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00000010.
        /// </summary>
        Reserved0x00000010 = 0x00000010,

        /// <summary>
        /// Indicates that the section contains executable code.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_CNT_CODE</remarks>
        Code = 0x00000020,

        /// <summary>
        /// Indicates that the section contains initialized data.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_CNT_INITIALIZED_DATA</remarks>
        InitializedData = 0x00000040,

        /// <summary>
        /// Indicates that the section contains uninitialized data.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_CNT_UNINITIALIZED_DATA</remarks>
        UninitializedData = 0x00000080,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00000100.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_LNK_OTHER</remarks>
        LinkerOther = 0x00000100,

        /// <summary>
        /// Indicates that the section contains comments or other information. The .drectve section has this type. This
        /// is valid for object files only.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_LNK_INFO</remarks>
        LinkerInformation = 0x00000200,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00000400.
        /// </summary>
        Reserved0x00000400 = 0x00000400,

        /// <summary>
        /// Indicates that the section will not become part of the image. This is valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_LNK_REMOVE</remarks>
        LinkerRemove = 0x00000800,

        /// <summary>
        /// Indicates that the section contains COMDAT data. This is valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_LNK_COMDAT</remarks>
        LinkerComDat = 0x00001000,

        /// <summary>
        /// Indicates an undocumented flag of the value 0x00002000.
        /// </summary>
        Unknown0x00002000 = 0x00002000,

        /// <summary>
        /// Indicates an undocumented flag of the value 0x00004000.
        /// </summary>
        Unknown0x00004000 = 0x00004000,

        /// <summary>
        /// Indicates that the section contains data referenced through the global pointer (GP).
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_GPREL</remarks>
        GlobalPointerData = 0x00008000,

        /// <summary>
        /// Indicates an undocumented flag of the value 0x00010000.
        /// </summary>
        Unknown0x00010000 = 0x00010000,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00020000.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_PURGEABLE</remarks>
        Purgeable = 0x00020000,

        /// <summary>
        /// Indicates for ARM machine types, that the section contains Thumb code. This is reserved for future use with
        /// other machine types.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_16BIT</remarks>
        ArmThumbCode = 0x00020000,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00040000.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_LOCKED</remarks>
        Locked = 0x00040000,

        /// <summary>
        /// Indicates a reserved flag of the value 0x00080000.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_PRELOAD</remarks>
        Preloadable = 0x00080000,

        /// <summary>
        /// Indicates that data is aligned on a 1-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_1BYTES</remarks>
        Align1Bytes = 0x00100000,

        /// <summary>
        /// Indicates that data is aligned on a 2-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_2BYTES</remarks>
        Align2Bytes = 0x00200000,

        /// <summary>
        /// Indicates that data is aligned on a 4-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_4BYTES</remarks>
        Align4Bytes = 0x00300000,

        /// <summary>
        /// Indicates that data is aligned on a 8-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_8BYTES</remarks>
        Align8Bytes = 0x00400000,

        /// <summary>
        /// Indicates that data is aligned on a 16-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_16BYTES</remarks>
        Align16Bytes = 0x00500000,

        /// <summary>
        /// Indicates that data is aligned on a 32-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_32BYTES</remarks>
        Align32Bytes = 0x00600000,

        /// <summary>
        /// Indicates that data is aligned on a 64-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_64BYTES</remarks>
        Align64Bytes = 0x00700000,

        /// <summary>
        /// Indicates that data is aligned on a 128-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_128BYTES</remarks>
        Align128Bytes = 0x00800000,

        /// <summary>
        /// Indicates that data is aligned on a 256-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_256BYTES</remarks>
        Align256Bytes = 0x00900000,

        /// <summary>
        /// Indicates that data is aligned on a 512-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_512BYTES</remarks>
        Align512Bytes = 0x00A00000,

        /// <summary>
        /// Indicates that data is aligned on a 1024-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_1024BYTES</remarks>
        Align1024Bytes = 0x00B00000,

        /// <summary>
        /// Indicates that data is aligned on a 2048-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_2048BYTES</remarks>
        Align2048Bytes = 0x00C00000,

        /// <summary>
        /// Indicates that data is aligned on a 4096-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_4096BYTES</remarks>
        Align4096Bytes = 0x00D00000,

        /// <summary>
        /// Indicates that data is aligned on a 8192-byte boundary. Valid only for object files.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_ALIGN_8192BYTES</remarks>
        Align8192Bytes = 0x00E00000,

        /// <summary>
        /// Indicates that the section contains extended relocations.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_LNK_NRELOC_OVFL</remarks>
        ExtendedRelocations = 0x01000000,

        /// <summary>
        /// Indicates that the section can be discarded as needed.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_DISCARDABLE</remarks>
        Discardable = 0x02000000,

        /// <summary>
        /// Indicates that the section cannot be cached.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_NOT_CACHED</remarks>
        NotCacheable = 0x04000000,

        /// <summary>
        /// Indicates that the section is not pageable.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_NOT_PAGED</remarks>
        NotPageable = 0x08000000,

        /// <summary>
        /// Indicates that the section can be shared in memory.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_SHARED</remarks>
        Shareable = 0x10000000,

        /// <summary>
        /// Indicates that the section can be executed as code.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_EXECUTE</remarks>
        Executable = 0x20000000,

        /// <summary>
        /// Indicates that the section can be read.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_READ</remarks>
        Readable = 0x40000000,

        /// <summary>
        /// Indicates that the section can be written to.
        /// </summary>
        /// <remarks>Native constant: IMAGE_SCN_MEM_WRITE</remarks>
        Writable = 0x80000000
    }
}
