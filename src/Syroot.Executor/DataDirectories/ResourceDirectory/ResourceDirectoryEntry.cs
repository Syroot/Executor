﻿using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents the common base for a <see cref="ResourceNode"/> or <see cref="ResourceLeaf"/>.
    /// </summary>
    public class ResourceDirectoryEntry
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the ID of the resource directory entry. This is 0 if a name is used instead.
        /// </summary>
        public uint Id { get; private set; }

        /// <summary>
        /// Gets the RVA (relative to the start of resource directory) header pointing to the name of the resource
        /// directory entry. This is 0 if an ID is used instead.
        /// </summary>
        public uint NameRva { get; private set; }

        /// <summary>
        /// Gets the resolved name of the resource directory entry. This is null if an ID is used instead.
        /// </summary>
        public string Name { get; private set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Instantiates a <see cref="ResourceDirectoryEntry"/> instance from the given <see cref="Stream"/>.
        /// Additional parsing information is used from the specified <see cref="PEFile"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        /// <param name="header">A <see cref="DataDirectoryHeader"/> providing parsing information.</param>
        /// <param name="named">True, if this is a named entry.</param>
        /// <returns>A new <see cref="ResourceDirectoryEntry"/> instance.</returns>
        internal static ResourceDirectoryEntry Instantiate(Stream stream, PEFile peFile,
            DataDirectoryHeader header, bool named)
        {
            ResourceDirectoryEntry entry;

            // NameRva / IntegerId
            uint idOrNameRva = stream.ReadUInt32();

            // DataEntryRva / SubdirectoryRva (relative to the resource directory start, not the file)
            uint dataRva = stream.ReadUInt32();
            uint offset = peFile.GetFileOffset(header.Rva) + (dataRva & 0x7FFFFFFF);
            if ((dataRva & 0x80000000) == 0x80000000)
            {
                // If the high bit is set, the remaining bits point to a sub node.
                using (new SeekTask(stream, offset, SeekOrigin.Begin))
                    entry = new ResourceNode(stream, peFile, header);
            }
            else
            {
                // If the high bit is clear, this points to a resource data entry.
                using (new SeekTask(stream, offset, SeekOrigin.Begin))
                    entry = new ResourceLeaf(stream, peFile);
            }

            // Resolve the name or get the ID and assign it to the entry.
            if (named)
            {
                entry.NameRva = idOrNameRva;
                uint nameOffset = peFile.GetFileOffset(header.Rva) + (entry.NameRva & 0x7FFFFFFF);
                using (new SeekTask(stream, nameOffset, SeekOrigin.Begin))
                    entry.Name = stream.ReadString(StringCoding.Int16CharCount, Encoding.Unicode);
            }
            else
            {
                entry.Id = idOrNameRva;
            }

            return entry;
        }
    }
}
