﻿using System.Diagnostics;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a leaf in the <see cref="ResourceDataDirectory"/>.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    public class ResourceLeaf : ResourceDirectoryEntry
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceLeaf"/> class, reading data from the given
        /// <see cref="Stream"/>. Additional parsing information is used from the specified <see cref="PEFile"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        internal ResourceLeaf(Stream stream, PEFile peFile)
        {
            DataRva = stream.ReadUInt32();
            DataSize = stream.ReadUInt32();
            CodePage = stream.ReadUInt32();

            // Read in the data pointed to.
            using (new SeekTask(stream, peFile.GetFileOffset(DataRva), SeekOrigin.Begin))
                Data = stream.ReadBytes((int)DataSize);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the RVA (relative to the start of resource directory) header pointing the resource data in the resource
        /// data area.
        /// </summary>
        public uint DataRva { get; }

        /// <summary>
        /// Gets the size in bytes of the resource data that is pointed to by the data RVA field.
        /// </summary>
        public uint DataSize { get; }

        /// <summary>
        /// Gets the code page that is used to decode code point values within the resource data. Typically, the code
        /// page would be Unicode.
        /// </summary>
        public uint CodePage { get; }

        /// <summary>
        /// Gets the data of the resource.
        /// </summary>
        public byte[] Data { get; }
    }
}
