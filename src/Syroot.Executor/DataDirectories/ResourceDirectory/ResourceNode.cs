﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a node in the <see cref="ResourceDataDirectory"/>.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    public class ResourceNode : ResourceDirectoryEntry
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceNode"/> class, reading data from the given
        /// <see cref="Stream"/>. Additional parsing information is used from the specified <see cref="PEFile"/>
        /// and <see cref="DataDirectoryHeader"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        /// <param name="header">A <see cref="DataDirectoryHeader"/> providing parsing information.</param>
        internal ResourceNode(Stream stream, PEFile peFile, DataDirectoryHeader header)
        {
            Characteristics = stream.ReadUInt32();
            TimeDateStamp = stream.ReadDateTime(DateTimeCoding.CTime);
            Version = new Version(stream.ReadUInt16(), stream.ReadUInt16());
            NameEntryCount = stream.ReadUInt16();
            IdEntryCount = stream.ReadUInt16();

            // The number of name entries follow, then the number of ID entries (all sorted).
            List<ResourceNode> subNodes = new List<ResourceNode>();
            List<ResourceLeaf> leaves = new List<ResourceLeaf>();
            for (int i = 0; i < NameEntryCount; i++)
            {
                ResourceDirectoryEntry directoryEntry = Instantiate(stream, peFile, header, true);
                AddEntryToLists(directoryEntry, subNodes, leaves);
            }
            for (int i = 0; i < IdEntryCount; i++)
            {
                ResourceDirectoryEntry directoryEntry = Instantiate(stream, peFile, header, false);
                AddEntryToLists(directoryEntry, subNodes, leaves);
            }
            Nodes = subNodes.AsReadOnly();
            Leaves = leaves.AsReadOnly();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a set of characteristics which is reserved and should be 0.
        /// </summary>
        public uint Characteristics { get; }

        /// <summary>
        /// Gets the time that the resource data was created by the resource compiler.
        /// </summary>
        public DateTime TimeDateStamp { get; }

        /// <summary>
        /// Gets the version set by the user.
        /// </summary>
        public Version Version { get; }

        /// <summary>
        /// Gets the number of directory entries that use strings to identify type, name or language entries.
        /// </summary>
        public uint NameEntryCount { get; }

        /// <summary>
        /// Gets the number of directory entries that use numeric IDs for type, name or language entries.
        /// </summary>
        public uint IdEntryCount { get; }

        /// <summary>
        /// Gets the list of sub nodes under this one.
        /// </summary>
        public ReadOnlyCollection<ResourceNode> Nodes { get; }

        /// <summary>
        /// Gets the list of leafs in this node.
        /// </summary>
        public ReadOnlyCollection<ResourceLeaf> Leaves { get; }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void AddEntryToLists(ResourceDirectoryEntry entry, List<ResourceNode> nodeList,
            List<ResourceLeaf> leafList)
        {
            switch (entry)
            {
                case ResourceNode node:
                    nodeList.Add(node);
                    break;
                case ResourceLeaf leaf:
                    leafList.Add(leaf);
                    break;
            }
        }
    }
}
