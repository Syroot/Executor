﻿using System.IO;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a data directory containing resources organized in nodes and leafs.
    /// </summary>
    public class ResourceDataDirectory : DataDirectoryBase
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceDataDirectory"/> class from the given
        /// <see cref="Stream"/>, using information specified in the <see cref="PEFile"/> and
        /// <see cref="DataDirectoryHeader"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the directory data from.</param>
        /// <param name="peFile">The <see cref="PEFile"/> instance to get RVA offsets from.</param>
        /// <param name="header">The <see cref="DataDirectoryHeader"/> providing information about the directory.
        /// </param>
        internal ResourceDataDirectory(Stream stream, PEFile peFile, DataDirectoryHeader header)
            : base(stream, peFile, header)
        {
            RootNode = new ResourceNode(stream, peFile, header);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="ResourceNode"/> forming the root of all subsequent <see cref="ResourceNode"/> instances
        /// and resources.
        /// </summary>
        public ResourceNode RootNode { get; }
    }
}
