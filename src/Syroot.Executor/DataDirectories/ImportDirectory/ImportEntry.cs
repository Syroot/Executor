﻿using System.Diagnostics;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents an import of another DLL.
    /// </summary>
    [DebuggerDisplay("Ordinal = {Ordinal}, Name = {Name}")]
    public class ImportEntry
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportEntry"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        internal ImportEntry(Stream stream, PEFile peFile)
        {
            // Get the flag which determines whether to import by ordinal or name.
            bool useOrdinal = false;
            ulong value;
            if (peFile.OptionalHeader.PEFormat == PEFormat.PE32Plus)
            {
                value = stream.ReadUInt64();
                useOrdinal = (value & 0x8000000000000000) == 0x8000000000000000;
            }
            else
            {
                value = stream.ReadUInt32();
                useOrdinal = (value & 0x80000000) == 0x80000000;
            }

            // Get the ordinal or name table RVA.
            if (useOrdinal)
            {
                // Import by this given ordinal.
                Ordinal = (ushort)(value & 0xFFFF);
            }
            else
            {
                // Import by name to which this RVA points to.
                NameTableRva = (uint)(value & 0x7FFFFFFF);
                // Fill in the names into the import entries.
                using (new SeekTask(stream, peFile.GetFileOffset(NameTableRva), SeekOrigin.Begin))
                {
                    Hint = stream.ReadUInt16();
                    Name = stream.ReadString(StringCoding.ZeroTerminated, Encoding.ASCII);
                }
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the imported functions ordinal in the export section of the imported DLL.
        /// </summary>
        public ushort Ordinal { get; }

        /// <summary>
        /// Gets the imported functions hint into the export section of the imported DLL.
        /// </summary>
        public ushort Hint { get; }

        /// <summary>
        /// Gets the RVA to the name table of the import section to get the imported function name from.
        /// </summary>
        public uint NameTableRva { get; }

        /// <summary>
        /// Gets the imported function name.
        /// </summary>
        public string Name { get; }
    }
}
