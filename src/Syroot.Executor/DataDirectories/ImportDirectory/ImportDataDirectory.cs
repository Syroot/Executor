﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a data directory containing an import table. It contains information about symbols imported from
    /// images through dynamic linking. Imported symbols are found in virtually all EXE files.
    /// </summary>
    public class ImportDataDirectory : DataDirectoryBase
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportDataDirectory"/> class from the given
        /// <see cref="Stream"/>, using information specified in the <see cref="PEFile"/> and
        /// <see cref="DataDirectoryHeader"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the directory data from.</param>
        /// <param name="peFile">The <see cref="PEFile"/> instance to get RVA offsets from.</param>
        /// <param name="header">The <see cref="DataDirectoryHeader"/> providing information about the directory.
        /// </param>
        internal ImportDataDirectory(Stream stream, PEFile peFile, DataDirectoryHeader header)
            : base(stream, peFile, header)
        {
            ReadImportTables(peFile, stream);
            ReadImportTableEntries(peFile, stream);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the list of <see cref="ImportTable">ImportTables</see> for each imported DLL.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public ReadOnlyCollection<ImportTable> ImportTables { get; private set; }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void ReadImportTables(PEFile peFile, Stream stream)
        {
            // Read in all the import directories for each DLL.
            List<ImportTable> importTables = new List<ImportTable>();
            while (true)
            {
                ImportTable importTable = new ImportTable(stream, peFile);
                // Check if this is the last, terminating entry.
                if (importTable.ImportLookupTableRva == 0)
                    break;
                importTables.Add(importTable);
            }
            ImportTables = importTables.AsReadOnly();
        }

        private void ReadImportTableEntries(PEFile peFile, Stream stream)
        {
            // Read the import entries of each import directory.
            foreach (ImportTable importTable in ImportTables)
            {
                List<ImportEntry> importEntries = new List<ImportEntry>();

                // Seek to the beginning of the import lookup table and read in all entries.
                stream.Seek(peFile.GetFileOffset(importTable.ImportLookupTableRva), SeekOrigin.Begin);
                while (true)
                {
                    ImportEntry importEntry = new ImportEntry(stream, peFile);
                    // Check if this is the last, terminating entry.
                    if (importEntry.Ordinal == 0 && importEntry.NameTableRva == 0)
                        break;
                    importEntries.Add(importEntry);
                }

                importTable.Entries = importEntries.AsReadOnly();
            }
        }
    }
}
