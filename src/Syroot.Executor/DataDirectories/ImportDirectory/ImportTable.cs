﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents an entry in the <see cref="ImportDataDirectory"/> describing the
    /// <see cref="ImportEntry">ImportEntries</see> of one DLL.
    /// </summary>
    [DebuggerDisplay("DllName = {DllName}, Count = {Entries.Count}")]
    public class ImportTable
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ImportTable"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        internal ImportTable(Stream stream, PEFile peFile)
        {
            // ImportLookupTableRVA
            ImportLookupTableRva = stream.ReadUInt32();
            TimeDateStamp = stream.ReadDateTime(DateTimeCoding.CTime);
            ForwarderChain = stream.ReadUInt32();
            DllNameRva = stream.ReadUInt32();

            // Get the name of the imported DLL.
            using (new SeekTask(stream, peFile.GetFileOffset(DllNameRva), SeekOrigin.Begin))
                DllName = stream.ReadString(StringCoding.ZeroTerminated, Encoding.ASCII);

            ImportAddressTableRva = stream.ReadUInt32();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the RVA of the import lookup table, which contains <see cref="ImportEntry"/> structures for each
        /// import.
        /// </summary>
        public uint ImportLookupTableRva { get; }

        /// <summary>
        /// Gets the time date stamp of the imported DLL set by the Windows loader when binding the image.
        /// </summary>
        public DateTime TimeDateStamp { get; }

        /// <summary>
        /// Gets the index of the first forwarder reference.
        /// </summary>
        public uint ForwarderChain { get; }

        /// <summary>
        /// Gets the RVA of the name of the imported DLL.
        /// </summary>
        public uint DllNameRva { get; }

        /// <summary>
        /// Gets the name of the imported DLL.
        /// </summary>
        public string DllName { get; }

        /// <summary>
        /// Gets the RVA of the import address table, while this one is changed when the image is loaded and bound.
        /// </summary>
        public uint ImportAddressTableRva { get; }

        /// <summary>
        /// Gets a list of import entries in this directory entry, representing the imported function either by ordinal
        /// or name.
        /// </summary>
        public ReadOnlyCollection<ImportEntry> Entries { get; internal set; }
    }
}
