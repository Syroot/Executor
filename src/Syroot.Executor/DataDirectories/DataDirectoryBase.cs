﻿using System;
using System.IO;
using System.Reflection;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents the base for data directory blocks in a PE file.
    /// </summary>
    public abstract class DataDirectoryBase
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="DataDirectoryBase"/> class from the given
        /// <see cref="Stream"/>, using information specified in the <see cref="DataDirectoryHeader"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the data directory data from.</param>
        /// <param name="peFile">The <see cref="PEFile"/> instance to enumerate the sections from.</param>
        /// <param name="header">The <see cref="DataDirectoryHeader"/> providing information about the section.</param>
        internal DataDirectoryBase(Stream stream, PEFile peFile, DataDirectoryHeader header)
        {
            // Jump to the start of the directory contents in the PE file.
            stream.Seek(peFile.GetFileOffset(header.Rva), SeekOrigin.Begin);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Instantiates a <see cref="DataDirectoryBase"/> instance matching the provided
        /// <see cref="DataDirectoryHeader"/>. Additional parsing information is used from the specified
        /// <see cref="PEFile"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        /// <param name="peFile">A <see cref="PEFile"/> instance providing parsing information.</param>
        /// <param name="directoryEntry">The <see cref="DataDirectoryEntry"/> index in the PE file under which this
        /// directory can be accessed.</param>
        /// <param name="directoryHeader">A <see cref="DataDirectoryHeader"/> providing the directory type information.
        /// </param>
        /// <returns>A <see cref="SectionHeader"/> implementing type instance matching the section header.</returns>
        internal static DataDirectoryBase Instantiate(Stream stream, PEFile peFile,
            DataDirectoryEntry directoryEntry, DataDirectoryHeader directoryHeader)
        {
            // Find the directory type matching the header.
            Type directoryType = FindType(directoryEntry);
            // TODO: Strongly type all directories.
            if (directoryType.IsAbstract)
                return null;

            // Find the constructor accepting the binary reader and section header.
            ConstructorInfo directoryConstructor = directoryType.GetConstructor(
                BindingFlags.Instance | BindingFlags.NonPublic, Type.DefaultBinder,
                new Type[] { typeof(Stream), typeof(PEFile), typeof(DataDirectoryHeader) }, null);

            // Invoke it with the given parameters and return the resulting instance.
            return (DataDirectoryBase)directoryConstructor.Invoke(new object[] { stream, peFile, directoryHeader });
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static Type FindType(DataDirectoryEntry directoryEntry)
        {
            // Get the type from the directory entry index.
            // TODO: Strongly type all directories.
            switch (directoryEntry)
            {
                case DataDirectoryEntry.ExportTable:
                    return typeof(ExportDataDirectory);
                case DataDirectoryEntry.ImportTable:
                    return typeof(ImportDataDirectory);
                case DataDirectoryEntry.ResourceTable:
                    return typeof(ResourceDataDirectory);
                case DataDirectoryEntry.ExceptionTable:
                case DataDirectoryEntry.CertificateTable:
                case DataDirectoryEntry.BaseRelocationTable:
                case DataDirectoryEntry.Debug:
                case DataDirectoryEntry.Architecture:
                case DataDirectoryEntry.GlobalPointer:
                case DataDirectoryEntry.ThreadLocalStorageTable:
                case DataDirectoryEntry.LoadConfigTable:
                case DataDirectoryEntry.BoundImportTable:
                case DataDirectoryEntry.ImportAddressTable:
                case DataDirectoryEntry.DelayImportDescriptor:
                case DataDirectoryEntry.ClrRuntimeHeader:
                case DataDirectoryEntry.Reserved:
                default:
                    return typeof(DataDirectoryBase);
            }
        }
    }
}
