﻿using System.Diagnostics;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a row in the <see cref="ExportDataDirectory"/> export table.
    /// </summary>
    [DebuggerDisplay("Ordinal = {Ordinal}, Name = {Name}")]
    public class ExportEntry
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the ordinal of the export, which is the index to the export address array in an ex
        /// </summary>
        public ushort Ordinal { get; internal set; }

        /// <summary>
        /// Gets the hint value for the exported function. The hint value is used internally by the operating system's
        /// loader to quickly match imports with exports. It is used as an index into the array of exported functions in
        /// the selected module.
        /// </summary>
        public uint Hint { get; internal set; }

        /// <summary>
        /// Gets the optional RVA of the name of the export. If this is 0, no name is defined for this export.
        /// </summary>
        public uint NameRva { get; internal set; }

        /// <summary>
        /// Gets the name of the export or null if no name has been defined.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Gets the RVA which points either to the code of the export or the RVA to the forwarder string.
        /// </summary>
        public uint CodeOrForwarderRva { get; internal set; }

        /// <summary>
        /// Gets the name of a forwarded export containing the DLL name, followed by a dot and either the name or the
        /// ordinal number of the export forwarded to. This can also be null if this is not a forwarded export.
        /// </summary>
        public string ForwarderName { get; internal set; }
    }
}
