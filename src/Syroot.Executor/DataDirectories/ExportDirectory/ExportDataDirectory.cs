﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents a data directory containing an export table. It contains information about symbols that other images
    /// can access through dynamic linking. Exported symbols are generally found in DLLs.
    /// </summary>
    public class ExportDataDirectory : DataDirectoryBase
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ExportDataDirectory"/> class from the given
        /// <see cref="Stream"/>, using information specified in the <see cref="DataDirectoryHeader"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to read the directory data from.</param>
        /// <param name="peFile">The <see cref="PEFile"/> instance to get RVA offsets from.</param>
        /// <param name="header">The <see cref="DataDirectoryHeader"/> providing information about the directory.
        /// </param>
        internal ExportDataDirectory(Stream stream, PEFile peFile, DataDirectoryHeader header)
            : base(stream, peFile, header)
        {
            ExportFlags = stream.ReadUInt32();
            TimeDateStamp = stream.ReadDateTime(DateTimeCoding.CTime);
            Version = new Version(stream.ReadUInt16(), stream.ReadUInt16());
            DllNameRva = stream.ReadUInt32();
            OrdinalStartNumber = stream.ReadUInt32();
            EntryCount = stream.ReadUInt32();
            NameEntryCount = stream.ReadUInt32();
            CodeAddressTableRva = stream.ReadUInt32();
            NameAddressTableRva = stream.ReadUInt32();
            OrdinalTableRva = stream.ReadUInt32();

            // DllName
            stream.Seek(peFile.GetFileOffset(DllNameRva), SeekOrigin.Begin);
            DllName = stream.ReadString(StringCoding.ZeroTerminated, Encoding.ASCII);

            ReadExportTables(peFile, stream, header);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a set of export flags which are reserved and should be 0.
        /// </summary>
        public uint ExportFlags { get; }

        /// <summary>
        /// Gets the time and date that the export data was created.
        /// </summary>
        public DateTime TimeDateStamp { get; }

        /// <summary>
        /// Gets the version number, which can be set by the user.
        /// </summary>
        public Version Version { get; }

        /// <summary>
        /// Gets the address of the ASCII string that contains the name of the DLL.
        /// </summary>
        public uint DllNameRva { get; }

        /// <summary>
        /// Gets the name referenced by <see cref="DllNameRva"/>.
        /// </summary>
        public string DllName { get; }

        /// <summary>
        /// Gets the starting ordinal number for exports in this image. It is usually set to 1.
        /// </summary>
        public uint OrdinalStartNumber { get; }

        /// <summary>
        /// Gets the number of entries in the export address table.
        /// </summary>
        public uint EntryCount { get; }

        /// <summary>
        /// Gets the number of entries in the name pointer table. This is also the number of entries in the ordinal
        /// table.
        /// </summary>
        public uint NameEntryCount { get; }

        /// <summary>
        /// Gets the RVA of the export address table containing pointers to the code of the exports.
        /// </summary>
        public uint CodeAddressTableRva { get; }

        /// <summary>
        /// Gets the RVA of the name pointer table containing optional pointers to names of exports.
        /// </summary>
        public uint NameAddressTableRva { get; }

        /// <summary>
        /// Gets the RVA of the ordinal table.
        /// </summary>
        public uint OrdinalTableRva { get; }

        /// <summary>
        /// Gets the exports defined in this PE file.
        /// </summary>
        public ReadOnlyCollection<ExportEntry> ExportEntries { get; private set; }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void ReadExportTables(PEFile peFile, Stream stream, DataDirectoryHeader header)
        {
            // Read the export address table which contains the RVAs to the exported code or forwarder names.
            ExportEntry[] exportEntries = new ExportEntry[EntryCount];
            stream.Seek(peFile.GetFileOffset(CodeAddressTableRva), SeekOrigin.Begin);
            for (int i = 0; i < exportEntries.Length; i++)
            {
                exportEntries[i] = new ExportEntry
                {
                    Ordinal = (ushort)(i + OrdinalStartNumber),
                    CodeOrForwarderRva = stream.ReadUInt32()
                };
            }

            // Read the ordinal table containing indices (with base) to named entries in the export entry table.
            stream.Seek(peFile.GetFileOffset(OrdinalTableRva), SeekOrigin.Begin);
            uint[] ordinals = new uint[NameEntryCount];
            for (int i = 0; i < ordinals.Length; i++)
            {
                // Get the name for the ordinal, which has the same index as the ordinal array element.
                ordinals[i] = stream.ReadUInt16();
            }

            // Read the export name pointer table which contains pointers to names of exports.
            stream.Seek(peFile.GetFileOffset(NameAddressTableRva), SeekOrigin.Begin);
            for (uint i = 0; i < ordinals.Length; i++)
            {
                exportEntries[ordinals[i]].Hint = i;
                exportEntries[ordinals[i]].NameRva = stream.ReadUInt32();
            }

            // Read the names of the exports or forwarders.
            for (int i = 0; i < exportEntries.Length; i++)
            {
                if (exportEntries[i].NameRva > 0)
                {
                    stream.Seek(peFile.GetFileOffset(exportEntries[i].NameRva), SeekOrigin.Begin);
                    exportEntries[i].Name = stream.ReadString(StringCoding.ZeroTerminated, Encoding.ASCII);
                }
                // Check if it's a forwarder export (the RVA points within the export directory to a forwarder name).
                if (exportEntries[i].CodeOrForwarderRva >= header.Rva
                    && exportEntries[i].CodeOrForwarderRva < header.Rva + header.Size)
                {
                    stream.Seek(peFile.GetFileOffset(exportEntries[i].CodeOrForwarderRva), SeekOrigin.Begin);
                    exportEntries[i].ForwarderName = stream.ReadString(StringCoding.ZeroTerminated, Encoding.ASCII);
                }
            }

            ExportEntries = new ReadOnlyCollection<ExportEntry>(exportEntries);
        }
    }
}
