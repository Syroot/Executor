﻿namespace Syroot.Executor
{
    /// <summary>
    /// Represents typical indices for specific data directories in the <see cref="PEFile.DataDirectoryHeaders"/> and
    /// <see cref="PEFile.DataDirectories"/> collections.
    /// </summary>
    /// <remarks>Native constants: IMAGE_DIRECTORY_ENTRY_*</remarks>
    public enum DataDirectoryEntry : int
    {
        /// <summary>
        /// Indicates the export table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_EXPORT</remarks>
        ExportTable = 0,

        /// <summary>
        /// Indicates the import table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_IMPORT</remarks>
        ImportTable = 1,

        /// <summary>
        /// Indicates the resource table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_RESOURCE</remarks>
        ResourceTable = 2,

        /// <summary>
        /// Indicates the exception table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_EXCEPTION</remarks>
        ExceptionTable = 3,

        /// <summary>
        /// Indicates the attribute certificate table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_SECURITY</remarks>
        CertificateTable = 4,

        /// <summary>
        /// Indicates the base relocation table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_BASERELOC</remarks>
        BaseRelocationTable = 5,

        /// <summary>
        /// Indicates the debug data starting address address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_DEBUG</remarks>
        Debug = 6,

        /// <summary>
        /// Indicates the architecture <see cref="DataDirectoryHeader"/>, which is not used for current machine types
        /// and must be 0.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_ARCHITECTURE</remarks>
        Architecture = 7,

        /// <summary>
        /// Indicates the RVA of the value to be stored in the global pointer register. The size must be set to 0.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_GLOBALPTR</remarks>
        GlobalPointer = 8,

        /// <summary>
        /// Indicates the TLS table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_TLS</remarks>
        ThreadLocalStorageTable = 9,

        /// <summary>
        /// Indicates the load configuration table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG</remarks>
        LoadConfigTable = 10,

        /// <summary>
        /// Indicates the bound import table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT</remarks>
        BoundImportTable = 11,

        /// <summary>
        /// Indicates the import address table address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_IAT</remarks>
        ImportAddressTable = 12,

        /// <summary>
        /// Indicates the delay import descriptor address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT</remarks>
        DelayImportDescriptor = 13,

        /// <summary>
        /// Indicates the CLR runtime header address and size.
        /// </summary>
        /// <remarks>Native constant: IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR</remarks>
        ClrRuntimeHeader = 14,

        /// <summary>
        /// Indicates a reserved <see cref="DataDirectoryHeader"/> address and size and must be 0.
        /// </summary>
        Reserved = 15
    }
}
