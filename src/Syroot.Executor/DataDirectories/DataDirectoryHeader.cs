﻿using System.Diagnostics;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.Executor
{
    /// <summary>
    /// Represents an address and size to a table or string that Windows uses. It is loaded into memory for fast access
    /// at runtime.
    /// </summary>
    [DebuggerDisplay("Rva = {Rva}, Size = {Size}")]
    public class DataDirectoryHeader
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="DataDirectoryHeader"/> class, reading data from the given
        /// <see cref="Stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to read the data from.</param>
        internal DataDirectoryHeader(Stream stream)
        {
            Rva = stream.ReadUInt32();
            Size = stream.ReadUInt32();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the RVA of where the table starts.
        /// </summary>
        public uint Rva { get; }

        /// <summary>
        /// Gets the size, in bytes, of the directory.
        /// </summary>
        public uint Size { get; }
    }
}
